﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    class Acceso
    {
        private SqlConnection conexion;

        public bool Abrir()//método abrir la conexión
        {
            bool ok;
            //valido si estoy conectado poniendolo en falso y si no es así conecto en el else
            if (conexion != null && conexion.State == System.Data.ConnectionState.Open)
            {
                ok = true;
            }
            else
            {
                conexion = new SqlConnection();

                //capturo el error en el caso que no pueda conectarme.
                try
                {
                    //declaro la conexión.
                    conexion.ConnectionString = @"Data Source =.\SqlExpress; Initial Catalog=Escoba; Integrated Security= SSPI";
                    conexion.Open();
                    ok = true;

                }
                catch (Exception ex)
                {

                    ok = false;
                }
            }
            return ok;


        }

        public void Cerrar()
        //Metodo cerrar si esta conectado lo cierra directamente.
        {
            if (conexion != null)
            {
                conexion.Close();
                conexion.Dispose();
                conexion = null;
                GC.Collect();
            }
        }

        //método para escribir, sqlComand para pasarle la sintaxis de sql
        //no se poruqe capturo -1 -2 verificar en video

        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {

            SqlCommand comando = new SqlCommand(sql);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            comando.Connection = conexion;
            return comando;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Double;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Date;
            return p;
        }
        public int Escribir(string sql, List<SqlParameter> parameters = null)
        {
            SqlCommand comando = CrearComando(sql, parameters, CommandType.StoredProcedure);


            int filasAfectadas = 0;
            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch
            {
                filasAfectadas = -1;
            }

            return filasAfectadas;
        }

        public DataTable Leer(string sql, List<SqlParameter> parameters = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql, parameters);

            DataTable Tabla = new DataTable();

            adaptador.Fill(Tabla);


            return Tabla;
        }
    }
}
