﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    class Naipe
    {
        private int numero;

        public int NUMERO
        {
            get { return numero; }
            set { numero = value; }
        }

        private int valor;

        public int VALOR
        {
            get { return valor; }
            set { valor = value; }
        }

        private bool estado;

        public bool Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        private Tipo_Naipe palo;

        public Tipo_Naipe PALO
        {
            get { return palo; }
            set { palo = value; }
        }

    }
}
